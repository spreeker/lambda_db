package main

import (
	"fmt"
	"strconv"
	"strings"
)

type registerCustomGroupByFunc map[string]func(*Item, ItemsGroupedBy)

var RegisterGroupByCustom registerCustomGroupByFunc

func (r registerCustomGroupByFunc) Options() string {
	options := []string{}
	for k := range r {
		options = append(options, k)
	}
	return strings.Join(options, ",")
}

func init() {
	RegisterGroupByCustom = make(registerCustomGroupByFunc)
	RegisterGroupByCustom["gebruiksdoelen-mixed"] = GroupByGettersGebruiksdoelen
	RegisterGroupByCustom["postcodehuisnummer"] = GroupByGettersPostcodehuisnummer
}

func reduceWEQ(items Items) map[string]string {
	result := make(map[string]string)
	weq := 0
	for i := range items {
		//_weq, err := strconv.ParseInt(items[i].Woningequivalent, 10, 64)
		//if err != nil {
		//	continue
		//}
		weq += int(items[i].Woningequivalent)
	}
	result["woningenquivalent"] = strconv.Itoa(weq)
	return result
}

func reduceEAN(items Items) map[string]string {
	result := make(map[string]string)
	ean := 0

	for i := range items {
		ean += int(items[i].GasEanCount)
	}

	result["eancodes"] = strconv.Itoa(ean)
	return result
}

func gettersToevoegingen(i *Item) string {
	return fmt.Sprintf("%s %d", i.Postcode, i.Huisnummer)
}

// getter Gebruiksdoelen
func GroupByGettersGebruiksdoelen(item *Item, grouping ItemsGroupedBy) {

	for i := range item.Gebruiksdoelen {
		groupkey := Gebruiksdoelen.GetValue(item.Gebruiksdoelen[i])
		grouping[groupkey] = append(grouping[groupkey], item)
	}
}

func GroupByGettersPostcodehuisnummer(item *Item, grouping ItemsGroupedBy) {
	groupkey := gettersToevoegingen(item)
	grouping[groupkey] = append(grouping[groupkey], item)
}

func GetAdres(i *Item) string {
	adres := fmt.Sprintf("%s %s %s %s %s %s",
		i.Straat,
		Huisnummer.GetValue(i.Huisnummer),
		Huisletter.GetValue(i.Huisletter),
		Huisnummertoevoeging.GetValue(i.Huisnummertoevoeging),
		i.Postcode,
		Gemeentenaam.GetValue(i.Gemeentenaam),
	)

	adres = strings.ReplaceAll(adres, "  ", " ")
	return adres
}
