module lambdadb

go 1.15

require (
	github.com/Attumm/settingo v1.5.0
	github.com/JensRantil/go-csv v0.0.0-20200923162218-7ffda755f61b
	github.com/Workiva/go-datastructures v1.0.52
	github.com/cheggaaa/pb v1.0.29
	github.com/go-spatial/geom v0.0.0-20200810200216-9bf4204b30e9
	github.com/goccy/go-json v0.10.2
	github.com/golang/geo v0.0.0-20200730024412-e86565bf3f35
	github.com/klauspost/compress v1.17.0 // indirect
	github.com/klauspost/pgzip v1.2.6
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.16.0
	golang.org/x/net v0.7.0
	zgo.at/goatcounter v1.4.2
)
