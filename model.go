/*
	model.go define the 'items' to store.
	All columns with getters and setters are defined here.

	ItemIn, represent rows from the Input data
	Item, the compact item stored in memmory
	ItemOut, defines how and which fields are exported out
	of the API. It is possible to ignore input columns

	Repeated values are stored in maps with int numbers
	as keys.  Optionally bitarrays are created for reapeated
	column values to do fast bit-wise filtering.

	A S2 geo index in created for lat, lon values.

	Unique values are stored as-is.

	The generated codes leaves room to create custom
	index functions yourself to create an API with an
	< 1 ms response time for your specific needs.

	This codebase solves: I need to have an API on this
	tabular dataset fast!
*/

package main

import (
	"encoding/json"
	"fmt"
	"sort"
	"strconv"
	"strings"

	"github.com/Workiva/go-datastructures/bitarray"
)

type registerGroupByFunc map[string]func(*Item) string
type registerGettersMap map[string]func(*Item) string
type registerReduce map[string]func(Items) map[string]string

func (r registerReduce) Options() string {
	options := []string{}
	for k := range r {
		options = append(options, k)
	}
	return strings.Join(options, ",")
}

func (r registerGroupByFunc) Options() string {
	options := []string{}
	for k := range r {
		options = append(options, k)
	}
	return strings.Join(options, ",")
}

type registerBitArray map[string]func(s string) (bitarray.BitArray, error)
type fieldBitarrayMap map[uint32]bitarray.BitArray

type ItemIn struct {
	Numid                                         string `json:"numid"`
	Pid                                           string `json:"pid"`
	Vid                                           string `json:"vid"`
	Lid                                           string `json:"lid"`
	Sid                                           string `json:"sid"`
	Postcode                                      string `json:"postcode"`
	Straat                                        string `json:"straat"`
	Huisnummer                                    string `json:"huisnummer"`
	Huisletter                                    string `json:"huisletter"`
	Huisnummertoevoeging                          string `json:"huisnummertoevoeging"`
	Oppervlakte                                   string `json:"oppervlakte"`
	Woningequivalent                              string `json:"woningequivalent"`
	Gebruiksdoelen                                string `json:"gebruiksdoelen"`
	PandBouwjaar                                  string `json:"pand_bouwjaar"`
	Pc6GemiddeldeWozWaardeWoning                  string `json:"gemiddelde_woz_waarde_woning"`
	GemiddeldeGemeenteWoz                         string `json:"gemiddelde_gemeente_woz"`
	Pc6EigendomssituatiePercKoop                  string `json:"pc6_eigendomssituatie_perc_koop"`
	Pc6EigendomssituatiePercHuur                  string `json:"pc6_eigendomssituatie_perc_huur"`
	Pc6EigendomssituatieAantalWoningenCorporaties string `json:"pc6_eigendomssituatie_aantal_woningen_corporaties"`
	Energieklasse                                 string `json:"energieklasse"`
	WoningType                                    string `json:"woning_type"`
	PandGebruiksoppervlakte                       string `json:"pand_gebruiksoppervlakte"`
	Sbicode                                       string `json:"sbicode"`
	GasEanCount                                   string `json:"eancount"`
	Pc6GroupId2022                                string `json:"group_id_2022"`
	P6Gasm32022                                   string `json:"p6_gasm3_2022"`
	P6Kwh2022                                     string `json:"p6_kwh_2022"`
	KwhLeveringsrichting2022                      string `json:"kwh_leveringsrichting_2022"`
	P6GrondbeslagM2                               string `json:"p6_grondbeslag_m2"`
	P6GasAansluitingen2022                        string `json:"p6_gas_aansluitingen_2022"`
	Point                                         string `json:"point"`
	Buurtcode                                     string `json:"buurtcode"`
	Buurtnaam                                     string `json:"buurtnaam"`
	Wijkcode                                      string `json:"wijkcode"`
	Wijknaam                                      string `json:"wijknaam"`
	Gemeentecode                                  string `json:"gemeentecode"`
	Gemeentenaam                                  string `json:"gemeentenaam"`
	Provincienaam                                 string `json:"provincienaam"`
	Provinciecode                                 string `json:"provinciecode"`
}

type ItemOut struct {
	Numid                                         int64  `json:"numid"`
	Pid                                           string `json:"pid"`
	Vid                                           int64  `json:"vid"`
	Lid                                           int64  `json:"lid"`
	Sid                                           int64  `json:"sid"`
	Postcode                                      string `json:"postcode"`
	Straat                                        string `json:"straat"`
	Huisnummer                                    string `json:"huisnummer"`
	Huisletter                                    string `json:"huisletter"`
	Huisnummertoevoeging                          string `json:"huisnummertoevoeging"`
	Oppervlakte                                   int64  `json:"oppervlakte"`
	Woningequivalent                              int64  `json:"woningequivalent"`
	Gebruiksdoelen                                string `json:"gebruiksdoelen"`
	PandBouwjaar                                  int64  `json:"pand_bouwjaar"`
	Pc6GemiddeldeWozWaardeWoning                  int64  `json:"gemiddelde_woz_waarde_woning"`
	GemiddeldeGemeenteWoz                         string `json:"gemiddelde_gemeente_woz"`
	Pc6EigendomssituatiePercKoop                  string `json:"pc6_eigendomssituatie_perc_koop"`
	Pc6EigendomssituatiePercHuur                  string `json:"pc6_eigendomssituatie_perc_huur"`
	Pc6EigendomssituatieAantalWoningenCorporaties string `json:"pc6_eigendomssituatie_aantal_woningen_corporaties"`
	Energieklasse                                 string `json:"energieklasse"`
	WoningType                                    string `json:"woning_type"`
	PandGebruiksoppervlakte                       int64  `json:"pand_gebruiksoppervlakte"`
	Sbicode                                       string `json:"sbicode"`
	GasEanCount                                   int64  `json:"eancount"`
	Pc6GroupId2022                                string `json:"group_id_2022"`
	P6Gasm32022                                   int64  `json:"p6_gasm3_2022"`
	P6Kwh2022                                     int64  `json:"p6_kwh_2022"`
	KwhLeveringsrichting2022                      int64  `json:"kwh_leveringsrichting_2022"`
	P6GrondbeslagM2                               int64  `json:"p6_grondbeslag_m2"`
	P6GasAansluitingen2022                        int64  `json:"p6_gas_aansluitingen_2022"`
	Point                                         string `json:"point"`
	Buurtcode                                     string `json:"buurtcode"`
	Buurtnaam                                     string `json:"buurtnaam"`
	Wijkcode                                      string `json:"wijkcode"`
	Wijknaam                                      string `json:"wijknaam"`
	Gemeentecode                                  string `json:"gemeentecode"`
	Gemeentenaam                                  string `json:"gemeentenaam"`
	Provincienaam                                 string `json:"provincienaam"`
	Provinciecode                                 string `json:"provinciecode"`
}

type Item struct {
	Label                                         int // internal index in ITEMS
	Numid                                         int64
	Pid                                           uint32
	Vid                                           int64
	Lid                                           int64
	Sid                                           int64
	Postcode                                      string
	Straat                                        string
	Huisnummer                                    uint32
	Huisletter                                    uint32
	Huisnummertoevoeging                          uint32
	Oppervlakte                                   int64
	Woningequivalent                              int64
	Gebruiksdoelen                                []uint32
	PandBouwjaar                                  int64
	Pc6GemiddeldeWozWaardeWoning                  int64
	GemiddeldeGemeenteWoz                         uint32
	Pc6EigendomssituatiePercKoop                  string
	Pc6EigendomssituatiePercHuur                  string
	Pc6EigendomssituatieAantalWoningenCorporaties string
	Energieklasse                                 uint32
	WoningType                                    uint32
	PandGebruiksoppervlakte                       int64
	Sbicode                                       uint32
	GasEanCount                                   int64
	Pc6GroupId2022                                uint32
	P6Gasm32022                                   int64
	P6Kwh2022                                     int64
	KwhLeveringsrichting2022                      int64
	P6GrondbeslagM2                               int64
	P6GasAansluitingen2022                        int64
	Point                                         string
	Buurtcode                                     uint32
	Buurtnaam                                     uint32
	Wijkcode                                      uint32
	Wijknaam                                      uint32
	Gemeentecode                                  uint32
	Gemeentenaam                                  uint32
	Provincienaam                                 uint32
	Provinciecode                                 uint32
}

func (i Item) MarshalJSON() ([]byte, error) {
	return json.Marshal(i.Serialize())
}

// Shrink create smaller Item using uint32
func (i ItemIn) Shrink(label int) Item {

	Pid.Store(i.Pid)
	Huisnummer.Store(i.Huisnummer)
	Huisletter.Store(i.Huisletter)
	Huisnummertoevoeging.Store(i.Huisnummertoevoeging)
	// Gebruiksdoelen.Store(i.Gebruiksdoelen)
	GemiddeldeGemeenteWoz.Store(i.GemiddeldeGemeenteWoz)
	Energieklasse.Store(i.Energieklasse)
	WoningType.Store(i.WoningType)
	Sbicode.Store(i.Sbicode)
	Pc6GroupId2022.Store(i.Pc6GroupId2022)
	Buurtcode.Store(i.Buurtcode)
	Buurtnaam.Store(i.Buurtnaam)
	Wijkcode.Store(i.Wijkcode)
	Wijknaam.Store(i.Wijknaam)
	Gemeentecode.Store(i.Gemeentecode)
	Gemeentenaam.Store(i.Gemeentenaam)
	Provincienaam.Store(i.Provincienaam)
	Provinciecode.Store(i.Provinciecode)

	doelen := Gebruiksdoelen.StoreArray(i.Gebruiksdoelen)

	numId, _ := strconv.ParseInt(i.Numid, 10, 64)
	// pid, _ := strconv.ParseInt(i.Pid, 10, 64)
	vid, _ := strconv.ParseInt(i.Vid, 10, 64)
	lid, _ := strconv.ParseInt(i.Lid, 10, 64)
	sid, _ := strconv.ParseInt(i.Sid, 10, 64)

	oppervlakte, _ := strconv.ParseInt(i.Oppervlakte, 10, 64)
	woningequivalent, _ := strconv.ParseInt(i.Woningequivalent, 10, 64)
	bouwjaar, _ := strconv.ParseInt(i.PandBouwjaar, 10, 64)

	// huisnummer, _ := strconv.ParseInt(i.Huisnummer, 10, 32)
	// Pc6EigendomssituatiePercKoop                  :=  string
	// Pc6EigendomssituatiePercHuur                  string
	// Pc6EigendomssituatieAantalWoningenCorporaties string

	eancount, _ := strconv.ParseInt(i.GasEanCount, 10, 64)
	gasm3, _ := strconv.ParseInt(i.P6Gasm32022, 10, 64)
	kwh, _ := strconv.ParseInt(i.P6Kwh2022, 10, 64)
	leveringsrichting, _ := strconv.ParseInt(i.KwhLeveringsrichting2022, 10, 64)
	m2, _ := strconv.ParseInt(i.P6GrondbeslagM2, 10, 64)
	p6aansluitingen, _ := strconv.ParseInt(i.P6GasAansluitingen2022, 10, 64)
	pandgebruiksoppervlakte, _ := strconv.ParseInt(i.PandGebruiksoppervlakte, 10, 64)
	gemiddelde_woz_waarde_woning, _ := strconv.ParseInt(i.Pc6GemiddeldeWozWaardeWoning, 10, 64)

	return Item{

		label,
		numId,
		Pid.GetIndex(i.Pid),
		vid,
		lid,
		sid,
		i.Postcode,
		i.Straat,
		Huisnummer.GetIndex(i.Huisnummer),
		// huisnummer,
		Huisletter.GetIndex(i.Huisletter),
		Huisnummertoevoeging.GetIndex(i.Huisnummertoevoeging),
		//i.Oppervlakte,
		oppervlakte,
		woningequivalent,
		doelen,
		//i.PandBouwjaar,
		bouwjaar,
		gemiddelde_woz_waarde_woning,
		GemiddeldeGemeenteWoz.GetIndex(i.GemiddeldeGemeenteWoz),
		i.Pc6EigendomssituatiePercKoop,
		i.Pc6EigendomssituatiePercHuur,
		i.Pc6EigendomssituatieAantalWoningenCorporaties,
		Energieklasse.GetIndex(i.Energieklasse),
		WoningType.GetIndex(i.WoningType),
		pandgebruiksoppervlakte,
		Sbicode.GetIndex(i.Sbicode),
		//i.Eancount,
		eancount,
		Pc6GroupId2022.GetIndex(i.Pc6GroupId2022),
		gasm3,             // i.P6Gasm32022,
		kwh,               // i.P6Kwh2022,
		leveringsrichting, // i.KwhLeveringsrichting2022,
		m2,                // i.P6GrondbeslagM2,
		p6aansluitingen,   // i.P6GasAansluitingen2022,
		i.Point,
		Buurtcode.GetIndex(i.Buurtcode),
		Buurtnaam.GetIndex(i.Buurtnaam),
		Wijkcode.GetIndex(i.Wijkcode),
		Wijknaam.GetIndex(i.Wijknaam),
		Gemeentecode.GetIndex(i.Gemeentecode),
		Gemeentenaam.GetIndex(i.Gemeentenaam),
		Provincienaam.GetIndex(i.Provincienaam),
		Provinciecode.GetIndex(i.Provinciecode),
	}
}

// Store selected columns in seperate map[columnvalue]bitarray
// for fast item indexed selection
func (i *Item) StoreBitArrayColumns() {
	SetBitArray("pid", i.Pid, i.Label)

	//SetBitArray("gebruiksdoelen", i.Gebruiksdoelen, i.Label)
	SetBitArray("buurtcode", i.Buurtcode, i.Label)
	SetBitArray("wijkcode", i.Wijkcode, i.Label)
	SetBitArray("gemeentecode", i.Gemeentecode, i.Label)
	SetBitArray("provincienaam", i.Provincienaam, i.Label)
	SetBitArray("provinciecode", i.Provinciecode, i.Label)

}

func (i Item) Serialize() ItemOut {
	return ItemOut{
		i.Numid,
		Pid.GetValue(i.Pid),
		i.Vid,
		i.Lid,
		i.Sid,
		i.Postcode,
		i.Straat,
		Huisnummer.GetValue(i.Huisnummer),
		Huisletter.GetValue(i.Huisletter),
		Huisnummertoevoeging.GetValue(i.Huisnummertoevoeging),

		i.Oppervlakte,
		i.Woningequivalent,
		Gebruiksdoelen.GetArrayValue(i.Gebruiksdoelen),
		i.PandBouwjaar,
		i.Pc6GemiddeldeWozWaardeWoning,
		GemiddeldeGemeenteWoz.GetValue(i.GemiddeldeGemeenteWoz),
		i.Pc6EigendomssituatiePercKoop,
		i.Pc6EigendomssituatiePercHuur,
		i.Pc6EigendomssituatieAantalWoningenCorporaties,
		Energieklasse.GetValue(i.Energieklasse),
		WoningType.GetValue(i.WoningType),
		i.PandGebruiksoppervlakte,
		Sbicode.GetValue(i.Sbicode),
		i.GasEanCount,
		Pc6GroupId2022.GetValue(i.Pc6GroupId2022),
		i.P6Gasm32022,
		i.P6Kwh2022,
		i.KwhLeveringsrichting2022,
		i.P6GrondbeslagM2,
		i.P6GasAansluitingen2022,
		i.Point,
		Buurtcode.GetValue(i.Buurtcode),
		Buurtnaam.GetValue(i.Buurtnaam),
		Wijkcode.GetValue(i.Wijkcode),
		Wijknaam.GetValue(i.Wijknaam),
		Gemeentecode.GetValue(i.Gemeentecode),
		Gemeentenaam.GetValue(i.Gemeentenaam),
		Provincienaam.GetValue(i.Provincienaam),
		Provinciecode.GetValue(i.Provinciecode),
	}
}

func (i ItemIn) Columns() []string {
	return []string{
		"numid",
		"pid",
		"vid",
		"lid",
		"sid",
		"postcode",
		"straat",
		"huisnummer",
		"huisletter",
		"huisnummertoevoeging",
		"oppervlakte",
		"woningequivalent",
		"gebruiksdoelen",
		"pand_bouwjaar",
		"gemiddelde_woz_waarde_woning",
		"gemiddelde_gemeente_woz",
		"pc6_eigendomssituatie_perc_koop",
		"pc6_eigendomssituatie_perc_huur",
		"pc6_eigendomssituatie_aantal_woningen_corporaties",
		"energieklasse",
		"woning_type",
		"pand_gebruiksoppervlakte",
		"sbicode",
		"eancount",
		"group_id_2022",
		"p6_gasm3_2022",
		"p6_kwh_2022",
		"kwh_leveringsrichting_2022",
		"p6_grondbeslag_m2",
		"p6_gas_aansluitingen_2022",
		"point",
		"buurtcode",
		"buurtnaam",
		"wijkcode",
		"wijknaam",
		"gemeentecode",
		"gemeentenaam",
		"provincienaam",
		"provinciecode",
	}
}

func (i ItemOut) Columns() []string {
	return []string{
		"numid",
		"pid",
		"vid",
		"lid",
		"sid",
		"postcode",
		"straat",
		"huisnummer",
		"huisletter",
		"huisnummertoevoeging",
		"oppervlakte",
		"woningequivalent",
		"gebruiksdoelen",
		"pand_bouwjaar",
		"gemiddelde_woz_waarde_woning",
		"gemiddelde_gemeente_woz",
		"pc6_eigendomssituatie_perc_koop",
		"pc6_eigendomssituatie_perc_huur",
		"pc6_eigendomssituatie_aantal_woningen_corporaties",
		"energieklasse",
		"woning_type",
		"pand_gebruiksoppervlakte",
		"sbicode",
		"eancount",
		"group_id_2022",
		"p6_gasm3_2022",
		"p6_kwh_2022",
		"kwh_leveringsrichting_2022",
		"p6_grondbeslag_m2",
		"p6_gas_aansluitingen_2022",
		"point",
		"buurtcode",
		"buurtnaam",
		"wijkcode",
		"wijknaam",
		"gemeentecode",
		"gemeentenaam",
		"provincienaam",
		"provinciecode",
	}
}

func (i Item) Row() []string {

	return []string{

		fmt.Sprintf("%d", i.Numid),
		Pid.GetValue(i.Pid),
		fmt.Sprintf("%d", i.Vid),
		fmt.Sprintf("%d", i.Lid),
		fmt.Sprintf("%d", i.Sid),

		i.Postcode,
		i.Straat,
		Huisnummer.GetValue(i.Huisnummer),
		Huisletter.GetValue(i.Huisletter),
		Huisnummertoevoeging.GetValue(i.Huisnummertoevoeging),
		// i.Oppervlakte,
		fmt.Sprintf("%d", i.Oppervlakte),
		fmt.Sprintf("%d", i.Woningequivalent),
		// Gebruiksdoelen.GetValue(i.Gebruiksdoelen),
		Gebruiksdoelen.GetArrayValue(i.Gebruiksdoelen),
		fmt.Sprintf("%d", i.PandBouwjaar),
		fmt.Sprintf("%d", i.Pc6GemiddeldeWozWaardeWoning),
		GemiddeldeGemeenteWoz.GetValue(i.GemiddeldeGemeenteWoz),
		i.Pc6EigendomssituatiePercKoop,
		i.Pc6EigendomssituatiePercHuur,
		i.Pc6EigendomssituatieAantalWoningenCorporaties,
		Energieklasse.GetValue(i.Energieklasse),
		WoningType.GetValue(i.WoningType),
		fmt.Sprintf("%d", i.PandGebruiksoppervlakte),
		Sbicode.GetValue(i.Sbicode),
		fmt.Sprintf("%d", i.GasEanCount),
		Pc6GroupId2022.GetValue(i.Pc6GroupId2022),
		fmt.Sprintf("%d", i.P6Gasm32022),
		fmt.Sprintf("%d", i.P6Kwh2022),
		fmt.Sprintf("%d", i.KwhLeveringsrichting2022),
		fmt.Sprintf("%d", i.P6GrondbeslagM2),
		fmt.Sprintf("%d", i.P6GasAansluitingen2022),
		i.Point,
		Buurtcode.GetValue(i.Buurtcode),
		Buurtnaam.GetValue(i.Buurtnaam),
		Wijkcode.GetValue(i.Wijkcode),
		Wijknaam.GetValue(i.Wijknaam),
		Gemeentecode.GetValue(i.Gemeentecode),
		Gemeentenaam.GetValue(i.Gemeentenaam),
		Provincienaam.GetValue(i.Provincienaam),
		Provinciecode.GetValue(i.Provinciecode),
	}
}

func (i Item) GetIndex() string {
	return GettersNumid(&i)
}

func (i Item) GetGeometry() string {
	return GettersPoint(&i)
}

// contain filter Numid
func FilterNumidContains(i *Item, s string) bool {
	is := fmt.Sprintf("%d", i.Numid)
	return strings.Contains(is, s)
}

// // startswith filter Numid
func FilterNumidStartsWith(i *Item, s string) bool {
	is := fmt.Sprintf("%d", i.Numid)
	return strings.HasPrefix(is, s)
}

// // match filters Numid
func FilterNumidMatch(i *Item, s string) bool {
	si, err := strconv.ParseInt(s, 10, 64)

	if err != nil {
		return false
	}

	return i.Numid == si
}

// getter Numid
func GettersNumid(i *Item) string {
	return fmt.Sprintf("%d", i.Numid)
}

// contain filter Pid
func FilterPidContains(i *Item, s string) bool {
	return strings.Contains(Pid.GetValue(i.Pid), s)
}

// // startswith filter Pid
// deal with leading 0
func FilterPidStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Pid.GetValue(i.Pid), s)
}

// match filters Pid
func FilterPidMatch(i *Item, s string) bool {
	return Pid.GetValue(i.Pid) == s
}

// getter Pid
func GettersPid(i *Item) string {
	return Pid.GetValue(i.Pid)
}

// contain filter Vid
func FilterVidContains(i *Item, s string) bool {
	is := fmt.Sprintf("%d", i.Vid)
	si, err := strconv.ParseInt(s, 10, 64)

	if err != nil {
		return false
	}
	ss := fmt.Sprintf("%d", si)

	return strings.Contains(is, ss)
}

// startswith filter Vid
func FilterVidStartsWith(i *Item, s string) bool {
	is := fmt.Sprintf("%d", i.Vid)
	return strings.HasPrefix(is, s)
}

// match filters Vid
func FilterVidMatch(i *Item, s string) bool {
	si, err := strconv.ParseInt(s, 10, 64)

	if err != nil {
		return false
	}

	return i.Vid == si
}

// getter Vid
func GettersVid(i *Item) string {
	return fmt.Sprintf("%d", i.Vid)
}

// match filters Lid
func FilterLidMatch(i *Item, s string) bool {
	si, err := strconv.ParseInt(s, 10, 64)

	if err != nil {
		return false
	}

	return i.Vid == si
}

// getter Lid
func GettersLid(i *Item) string {
	return fmt.Sprintf("%d", i.Lid)
}

// match filters Sid
func FilterSidMatch(i *Item, s string) bool {
	si, err := strconv.ParseInt(s, 10, 64)

	if err != nil {
		return false
	}

	return i.Sid == si
}

// getter Sid
func GettersSid(i *Item) string {
	return fmt.Sprintf("%d", i.Lid)
}

// contain filter Postcode
func FilterPostcodeContains(i *Item, s string) bool {
	return strings.Contains(i.Postcode, s)
}

// startswith filter Postcode
func FilterPostcodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Postcode, s)
}

// match filters Postcode
func FilterPostcodeMatch(i *Item, s string) bool {
	return i.Postcode == s
}

// getter Postcode
func GettersPostcode(i *Item) string {
	return i.Postcode
}

// contain filter Straat
func FilterStraatContains(i *Item, s string) bool {
	return strings.Contains(i.Straat, s)
}

// startswith filter Straat
func FilterStraatStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Straat, s)
}

// match filters Straat
func FilterStraatMatch(i *Item, s string) bool {
	return i.Straat == s
}

// getter Straat
func GettersStraat(i *Item) string {
	return i.Straat
}

// contain filter Huisnummer
func FilterHuisnummerContains(i *Item, s string) bool {
	return strings.Contains(Huisnummer.GetValue(i.Huisnummer), s)
}

// startswith filter Huisnummer
func FilterHuisnummerStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Huisnummer.GetValue(i.Huisnummer), s)
}

// match filters Huisnummer
func FilterHuisnummerMatch(i *Item, s string) bool {
	return Huisnummer.GetValue(i.Huisnummer) == s
}

// getter Huisnummer
func GettersHuisnummer(i *Item) string {
	return Huisnummer.GetValue(i.Huisnummer)
}

// contain filter Huisletter
func FilterHuisletterContains(i *Item, s string) bool {
	return strings.Contains(Huisletter.GetValue(i.Huisletter), s)
}

// startswith filter Huisletter
func FilterHuisletterStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Huisletter.GetValue(i.Huisletter), s)
}

// match filters Huisletter
func FilterHuisletterMatch(i *Item, s string) bool {
	return Huisletter.GetValue(i.Huisletter) == s
}

// getter Huisletter
func GettersHuisletter(i *Item) string {
	return Huisletter.GetValue(i.Huisletter)
}

// contain filter Huisnummertoevoeging
func FilterHuisnummertoevoegingContains(i *Item, s string) bool {
	return strings.Contains(Huisnummertoevoeging.GetValue(i.Huisnummertoevoeging), s)
}

// startswith filter Huisnummertoevoeging
func FilterHuisnummertoevoegingStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Huisnummertoevoeging.GetValue(i.Huisnummertoevoeging), s)
}

// match filters Huisnummertoevoeging
func FilterHuisnummertoevoegingMatch(i *Item, s string) bool {
	return Huisnummertoevoeging.GetValue(i.Huisnummertoevoeging) == s
}

// getter Huisnummertoevoeging
func GettersHuisnummertoevoeging(i *Item) string {
	return Huisnummertoevoeging.GetValue(i.Huisnummertoevoeging)
}

// contain filter Oppervlakte
//func FilterOppervlakteContains(i *Item, s string) bool {
//	return strings.Contains(i.Oppervlakte, s)
//}
//
//// startswith filter Oppervlakte
//func FilterOppervlakteStartsWith(i *Item, s string) bool {
//	return strings.HasPrefix(i.Oppervlakte, s)
//}
//
//// match filters Oppervlakte
//func FilterOppervlakteMatch(i *Item, s string) bool {
//	return i.Oppervlakte == s
//}

// getter Oppervlakte
func GettersOppervlakte(i *Item) string {
	return fmt.Sprintf("%d", i.Oppervlakte)
}

// contain filter Woningequivalent
//func FilterWoningequivalentContains(i *Item, s string) bool {
//	return strings.Contains(i.Woningequivalent, s)
//}
//
//// startswith filter Woningequivalent
//func FilterWoningequivalentStartsWith(i *Item, s string) bool {
//	return strings.HasPrefix(i.Woningequivalent, s)
//}

// match filters Woningequivalent
//func FilterWoningequivalentMatch(i *Item, s string) bool {
//	return i.Woningequivalent == s
//}

// getter Woningequivalent
func GettersWoningequivalent(i *Item) string {
	return fmt.Sprintf("%d", i.Woningequivalent)
}

// contain filter PandBouwjaar
//func FilterPandBouwjaarContains(i *Item, s string) bool {
//	return strings.Contains(i.PandBouwjaar, s)
//}
//
//// startswith filter PandBouwjaar
//func FilterPandBouwjaarStartsWith(i *Item, s string) bool {
//	return strings.HasPrefix(i.PandBouwjaar, s)
//}

// match filters PandBouwjaar
func FilterPandBouwjaarMatch(i *Item, s string) bool {
	si, err := strconv.ParseInt(s, 10, 64)

	if err != nil {
		return false
	}

	return i.PandBouwjaar == si
}

// contain filter GemiddeldeWozWaardeWoning
//func FilterGemiddeldeWozWaardeWoningContains(i *Item, s string) bool {
//	return strings.Contains(i.GemiddeldeWozWaardeWoning, s)
//}
//
//// startswith filter GemiddeldeWozWaardeWoning
//func FilterGemiddeldeWozWaardeWoningStartsWith(i *Item, s string) bool {
//	return strings.HasPrefix(i.GemiddeldeWozWaardeWoning, s)
//}

// match filters GemiddeldeWozWaardeWoning
//
//	func FilterGemiddeldeWozWaardeWoningMatch(i *Item, s string) bool {
//		return i.GemiddeldeWozWaardeWoning == s
//	}
//
// // getter GemiddeldeWozWaardeWoning
func GettersGemiddeldeWozWaardeWoning(i *Item) string {
	return fmt.Sprintf("%d", i.Pc6GemiddeldeWozWaardeWoning)
}

func GettersGemiddeldeGemeenteWoz(i *Item) string {
	return fmt.Sprintf("%d", i.GemiddeldeGemeenteWoz)
}

// contain filter GemiddeldeGemeenteWoz
func FilterGemiddeldeGemeenteWozContains(i *Item, s string) bool {
	return strings.Contains(GemiddeldeGemeenteWoz.GetValue(i.GemiddeldeGemeenteWoz), s)
}

// startswith filter GemiddeldeGemeenteWoz
func FilterGemiddeldeGemeenteWozStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(GemiddeldeGemeenteWoz.GetValue(i.GemiddeldeGemeenteWoz), s)
}

// getter PandBouwjaar
func GettersPandBouwjaar(i *Item) string {
	return fmt.Sprintf("%d", i.PandBouwjaar)
}

// startswith filter Pc6EigendomssituatiePercKoop
func FilterPc6EigendomssituatiePercKoopStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Pc6EigendomssituatiePercKoop, s)
}

// match filters Pc6EigendomssituatiePercKoop
func FilterPc6EigendomssituatiePercKoopMatch(i *Item, s string) bool {
	return i.Pc6EigendomssituatiePercKoop == s
}

// getter Pc6EigendomssituatiePercKoop
func GettersPc6EigendomssituatiePercKoop(i *Item) string {
	return i.Pc6EigendomssituatiePercKoop
}

// contain filter Pc6EigendomssituatiePercHuur
func FilterPc6EigendomssituatiePercHuurContains(i *Item, s string) bool {
	return strings.Contains(i.Pc6EigendomssituatiePercHuur, s)
}

// startswith filter Pc6EigendomssituatiePercHuur
func FilterPc6EigendomssituatiePercHuurStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Pc6EigendomssituatiePercHuur, s)
}

// match filters Pc6EigendomssituatiePercHuur
func FilterPc6EigendomssituatiePercHuurMatch(i *Item, s string) bool {
	return i.Pc6EigendomssituatiePercHuur == s
}

// getter Pc6EigendomssituatiePercHuur
func GettersPc6EigendomssituatiePercHuur(i *Item) string {
	return i.Pc6EigendomssituatiePercHuur
}

// contain filter Pc6EigendomssituatieAantalWoningenCorporaties
func FilterPc6EigendomssituatieAantalWoningenCorporatiesContains(i *Item, s string) bool {
	return strings.Contains(i.Pc6EigendomssituatieAantalWoningenCorporaties, s)
}

// startswith filter Pc6EigendomssituatieAantalWoningenCorporaties
func FilterPc6EigendomssituatieAantalWoningenCorporatiesStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Pc6EigendomssituatieAantalWoningenCorporaties, s)
}

// match filters Pc6EigendomssituatieAantalWoningenCorporaties
func FilterPc6EigendomssituatieAantalWoningenCorporatiesMatch(i *Item, s string) bool {
	return i.Pc6EigendomssituatieAantalWoningenCorporaties == s
}

// getter Pc6EigendomssituatieAantalWoningenCorporaties
func GettersPc6EigendomssituatieAantalWoningenCorporaties(i *Item) string {
	return i.Pc6EigendomssituatieAantalWoningenCorporaties
}

// contain filter Energieklasse
func FilterEnergieklasseContains(i *Item, s string) bool {
	return strings.Contains(Energieklasse.GetValue(i.Energieklasse), s)
}

// startswith filter Energieklasse
func FilterEnergieklasseStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Energieklasse.GetValue(i.Energieklasse), s)
}

// match filters Energieklasse
func FilterEnergieklasseMatch(i *Item, s string) bool {
	return Energieklasse.GetValue(i.Energieklasse) == s
}

// getter Energieklasse
func GettersEnergieklasse(i *Item) string {
	return Energieklasse.GetValue(i.Energieklasse)
}

// contain filter WoningType
func FilterWoningTypeContains(i *Item, s string) bool {
	return strings.Contains(WoningType.GetValue(i.WoningType), s)
}

// startswith filter WoningType
func FilterWoningTypeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(WoningType.GetValue(i.WoningType), s)
}

// match filters WoningType
func FilterWoningTypeMatch(i *Item, s string) bool {
	return WoningType.GetValue(i.WoningType) == s
}

// getter WoningType
func GettersWoningType(i *Item) string {
	return WoningType.GetValue(i.WoningType)
}

// contain filter PandGebruiksoppervlakte
// func FilterPandGebruiksoppervlakteContains(i *Item, s string) bool {
// 	return strings.Contains(i.PandGebruiksoppervlakte, s)
// }

// startswith filter PandGebruiksoppervlakte
// func FilterPandGebruiksoppervlakteStartsWith(i *Item, s string) bool {
// 	return strings.HasPrefix(i.PandGebruiksoppervlakte, s)
// }

// match filters PandGebruiksoppervlakte
// func FilterPandGebruiksoppervlakteMatch(i *Item, s string) bool {
// 	return i.PandGebruiksoppervlakte == s
// }

// getter PandGebruiksoppervlakte
func GettersPandGebruiksoppervlakte(i *Item) string {
	return fmt.Sprintf("%d", i.PandGebruiksoppervlakte)
}

// contain filter Sbicode
func FilterSbicodeContains(i *Item, s string) bool {
	return strings.Contains(Sbicode.GetValue(i.Sbicode), s)
}

// startswith filter Sbicode
func FilterSbicodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Sbicode.GetValue(i.Sbicode), s)
}

// match filters Sbicode
func FilterSbicodeMatch(i *Item, s string) bool {
	return Sbicode.GetValue(i.Sbicode) == s
}

// getter Sbicode
func GettersSbicode(i *Item) string {
	return Sbicode.GetValue(i.Sbicode)
}

// contain filter Eancount
//
//	func FilterEancountContains(i *Item, s string) bool {
//		return strings.Contains(i.Eancount, s)
//	}
//
// // startswith filter Eancount
//
//	func FilterEancountStartsWith(i *Item, s string) bool {
//		return strings.HasPrefix(i.Eancount, s)
//	}
//
// match filters Eancount
func FilterEancountMatch(i *Item, s string) bool {
	si, err := strconv.ParseInt(s, 10, 64)

	if err != nil {
		return false
	}

	return i.GasEanCount == si
}

func FilterEancountgte(i *Item, s string) bool {
	min, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return false
	}
	if i.GasEanCount <= min {
		return false
	}
	return true
}

// getter EanCount
func GettersGasEanCount(i *Item) string {
	return fmt.Sprintf("%d", i.GasEanCount)
}

// contain filter Pc6GroupId2022
func FilterPc6GroupId2022Contains(i *Item, s string) bool {
	return strings.Contains(Pc6GroupId2022.GetValue(i.Pc6GroupId2022), s)
}

// startswith filter Pc6GroupId2022
func FilterPc6GroupId2022StartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Pc6GroupId2022.GetValue(i.Pc6GroupId2022), s)
}

// match filters Pc6GroupId2022
func FilterPc6GroupId2022Match(i *Item, s string) bool {
	return Pc6GroupId2022.GetValue(i.Pc6GroupId2022) == s
}

// getter Pc6GroupId2022
func GettersPc6GroupId2022(i *Item) string {
	return Pc6GroupId2022.GetValue(i.Pc6GroupId2022)
}

// contain filter P6Gasm32022
//
//	func FilterP6Gasm32022Contains(i *Item, s string) bool {
//		return strings.Contains(i.P6Gasm32022, s)
//	}
func FilterP6Gasm32022lte(i *Item, s string) bool {
	max, err := strconv.Atoi(s)
	if err != nil {
		return false
	}
	if i.P6Gasm32022 >= int64(max) {
		return false
	}
	return true
}

func FilterP6Gasm32022gte(i *Item, s string) bool {
	min, err := strconv.Atoi(s)
	if err != nil {
		return false
	}
	if i.P6Gasm32022 <= int64(min) {
		return false
	}
	return true
}

//
//// startswith filter P6Gasm32022
//func FilterP6Gasm32022StartsWith(i *Item, s string) bool {
//	return strings.HasPrefix(i.P6Gasm32022, s)
//}
//
//// match filters P6Gasm32022
//func FilterP6Gasm32022Match(i *Item, s string) bool {
//	return i.P6Gasm32022 == s
//}

// getter P6Gasm32022
func GettersP6Gasm32022(i *Item) string {
	return fmt.Sprintf("%d", i.P6Gasm32022)
}

// contain filter P6Kwh2022
//func FilterP6Kwh2022Contains(i *Item, s string) bool {
//	return strings.Contains(i.P6Kwh2022, s)
//}
//
//// startswith filter P6Kwh2022
//func FilterP6Kwh2022StartsWith(i *Item, s string) bool {
//	return strings.HasPrefix(i.P6Kwh2022, s)
//}
//
//// match filters P6Kwh2022
//func FilterP6Kwh2022Match(i *Item, s string) bool {
//	return i.P6Kwh2022 == s
//}

// getter P6Kwh2022
func GettersP6Kwh2022(i *Item) string {
	return fmt.Sprintf("%d", i.P6Kwh2022)
}

// contain filter KwhLeveringsrichting2022
//func FilterKwhLeveringsrichting2022Contains(i *Item, s string) bool {
//	return strings.Contains(i.KwhLeveringsrichting2022, s)
//}
//
//// startswith filter KwhLeveringsrichting2022
//func FilterKwhLeveringsrichting2022StartsWith(i *Item, s string) bool {
//	return strings.HasPrefix(i.KwhLeveringsrichting2022, s)
//}

// match filters KwhLeveringsrichting2022
//func FilterKwhLeveringsrichting2022Match(i *Item, s string) bool {
//	return i.KwhLeveringsrichting2022 == s
//}

// getter KwhLeveringsrichting2022
func GettersKwhLeveringsrichting2022(i *Item) string {
	return fmt.Sprintf("%d", i.KwhLeveringsrichting2022)
}

// contain filter P6GrondbeslagM2
//func FilterP6GrondbeslagM2Contains(i *Item, s string) bool {
//	return strings.Contains(i.P6GrondbeslagM2, s)
//}
//
//// startswith filter P6GrondbeslagM2
//func FilterP6GrondbeslagM2StartsWith(i *Item, s string) bool {
//	return strings.HasPrefix(i.P6GrondbeslagM2, s)
//}
//
//// match filters P6GrondbeslagM2
//func FilterP6GrondbeslagM2Match(i *Item, s string) bool {
//	return i.P6GrondbeslagM2 == s
//}

// getter P6GrondbeslagM2
func GettersP6GrondbeslagM2(i *Item) string {
	return fmt.Sprintf("%d", i.P6GrondbeslagM2)
}

// contain filter P6GasAansluitingen2022
//func FilterP6GasAansluitingen2022Contains(i *Item, s string) bool {
//	return strings.Contains(i.P6GasAansluitingen2022, s)
//}
//
//// startswith filter P6GasAansluitingen2022
//func FilterP6GasAansluitingen2022StartsWith(i *Item, s string) bool {
//	return strings.HasPrefix(i.P6GasAansluitingen2022, s)
//}
//
//// match filters P6GasAansluitingen2022
//func FilterP6GasAansluitingen2022Match(i *Item, s string) bool {
//	return i.P6GasAansluitingen2022 == s
//}

// getter P6GasAansluitingen2022
func GettersP6GasAansluitingen2022(i *Item) string {
	return fmt.Sprintf("%d", i.P6GasAansluitingen2022)
}

// contain filter Point
func FilterPointContains(i *Item, s string) bool {
	return strings.Contains(i.Point, s)
}

// startswith filter Point
func FilterPointStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Point, s)
}

// match filters Point
func FilterPointMatch(i *Item, s string) bool {
	return i.Point == s
}

// getter Point
func GettersPoint(i *Item) string {
	return i.Point
}

// contain filter Buurtcode
func FilterBuurtcodeContains(i *Item, s string) bool {
	return strings.Contains(Buurtcode.GetValue(i.Buurtcode), s)
}

// startswith filter Buurtcode
func FilterBuurtcodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Buurtcode.GetValue(i.Buurtcode), s)
}

// match filters Buurtcode
func FilterBuurtcodeMatch(i *Item, s string) bool {
	return Buurtcode.GetValue(i.Buurtcode) == s
}

// getter Buurtcode
func GettersBuurtcode(i *Item) string {
	return Buurtcode.GetValue(i.Buurtcode)
}

// contain filter Buurtnaam
func FilterBuurtnaamContains(i *Item, s string) bool {
	return strings.Contains(Buurtnaam.GetValue(i.Buurtnaam), s)
}

// startswith filter Buurtnaam
func FilterBuurtnaamStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Buurtnaam.GetValue(i.Buurtnaam), s)
}

// match filters Buurtnaam
func FilterBuurtnaamMatch(i *Item, s string) bool {
	return Buurtnaam.GetValue(i.Buurtnaam) == s
}

// getter Buurtnaam
func GettersBuurtnaam(i *Item) string {
	return Buurtnaam.GetValue(i.Buurtnaam)
}

// contain filter Wijkcode
func FilterWijkcodeContains(i *Item, s string) bool {
	return strings.Contains(Wijkcode.GetValue(i.Wijkcode), s)
}

// startswith filter Wijkcode
func FilterWijkcodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Wijkcode.GetValue(i.Wijkcode), s)
}

// match filters Wijkcode
func FilterWijkcodeMatch(i *Item, s string) bool {
	return Wijkcode.GetValue(i.Wijkcode) == s
}

// getter Wijkcode
func GettersWijkcode(i *Item) string {
	return Wijkcode.GetValue(i.Wijkcode)
}

// contain filter Wijknaam
func FilterWijknaamContains(i *Item, s string) bool {
	return strings.Contains(Wijknaam.GetValue(i.Wijknaam), s)
}

// startswith filter Wijknaam
func FilterWijknaamStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Wijknaam.GetValue(i.Wijknaam), s)
}

// match filters Wijknaam
func FilterWijknaamMatch(i *Item, s string) bool {
	return Wijknaam.GetValue(i.Wijknaam) == s
}

// getter Wijknaam
func GettersWijknaam(i *Item) string {
	return Wijknaam.GetValue(i.Wijknaam)
}

// contain filter Gemeentecode
func FilterGemeentecodeContains(i *Item, s string) bool {
	return strings.Contains(Gemeentecode.GetValue(i.Gemeentecode), s)
}

// startswith filter Gemeentecode
func FilterGemeentecodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Gemeentecode.GetValue(i.Gemeentecode), s)
}

// match filters Gemeentecode
func FilterGemeentecodeMatch(i *Item, s string) bool {
	return Gemeentecode.GetValue(i.Gemeentecode) == s
}

// getter Gemeentecode
func GettersGemeentecode(i *Item) string {
	return Gemeentecode.GetValue(i.Gemeentecode)
}

// contain filter Gemeentenaam
func FilterGemeentenaamContains(i *Item, s string) bool {
	return strings.Contains(Gemeentenaam.GetValue(i.Gemeentenaam), s)
}

// startswith filter Gemeentenaam
func FilterGemeentenaamStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Gemeentenaam.GetValue(i.Gemeentenaam), s)
}

// match filters Gemeentenaam
func FilterGemeentenaamMatch(i *Item, s string) bool {
	return Gemeentenaam.GetValue(i.Gemeentenaam) == s
}

// getter Gemeentenaam
func GettersGemeentenaam(i *Item) string {
	return Gemeentenaam.GetValue(i.Gemeentenaam)
}

// contain filter Provincienaam
func FilterProvincienaamContains(i *Item, s string) bool {
	return strings.Contains(Provincienaam.GetValue(i.Provincienaam), s)
}

// startswith filter Provincienaam
func FilterProvincienaamStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Provincienaam.GetValue(i.Provincienaam), s)
}

// match filters Provincienaam
func FilterProvincienaamMatch(i *Item, s string) bool {
	return Provincienaam.GetValue(i.Provincienaam) == s
}

// getter Provincienaam
func GettersProvincienaam(i *Item) string {
	return Provincienaam.GetValue(i.Provincienaam)
}

// getter Provinciecode
func GettersProvinciecode(i *Item) string {
	return Provinciecode.GetValue(i.Provinciecode)
}

// contain filter Provinciecode
func FilterProvinciecodeContains(i *Item, s string) bool {
	return strings.Contains(Provinciecode.GetValue(i.Provinciecode), s)
}

// startswith filter Provinciecode
func FilterProvinciecodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Provinciecode.GetValue(i.Provinciecode), s)
}

// match filters Provinciecode
func FilterProvinciecodeMatch(i *Item, s string) bool {
	return Provinciecode.GetValue(i.Provinciecode) == s
}

// contain filter Gebruiksdoelen
func FilterGebruiksdoelenContains(i *Item, s string) bool {
	for _, v := range i.Gebruiksdoelen {
		vs := Gebruiksdoelen.GetValue(v)
		if strings.Contains(vs, s) {
			return true
		}
	}
	return false
}

// startswith filter Gebruiksdoelen
func FilterGebruiksdoelenStartsWith(i *Item, s string) bool {
	for _, v := range i.Gebruiksdoelen {
		vs := Gebruiksdoelen.GetValue(v)
		if strings.HasPrefix(vs, s) {
			return true
		}
	}
	return false

}

// match filters Gebruiksdoelen
func FilterGebruiksdoelenMatch(i *Item, s string) bool {
	for _, v := range i.Gebruiksdoelen {
		vs := Gebruiksdoelen.GetValue(v)
		if vs == s {
			return true
		}
	}
	return false
}

// getter Gebruiksdoelen
func GettersGebruiksdoelen(i *Item) string {
	return Gebruiksdoelen.GetArrayValue(i.Gebruiksdoelen)
}

/*
// contain filters
func FilterEkeyContains(i *Item, s string) bool {
	return strings.Contains(i.Ekey, s)
}


// startswith filters
func FilterEkeyStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Ekey, s)
}


// match filters
func FilterEkeyMatch(i *Item, s string) bool {
	return i.Ekey == s
}

// getters
func GettersEkey(i *Item) string {
	return i.Ekey
}
*/

// reduce functions
func reduceCount(items Items) map[string]string {
	result := make(map[string]string)
	result["count"] = strconv.Itoa(len(items))
	return result
}

type GroupedOperations struct {
	Funcs     registerFuncType
	GroupBy   registerGroupByFunc
	Getters   registerGettersMap
	Reduce    registerReduce
	BitArrays registerBitArray
}

var Operations GroupedOperations

var RegisterFuncMap registerFuncType
var RegisterGroupBy registerGroupByFunc
var RegisterGetters registerGettersMap
var RegisterReduce registerReduce
var RegisterBitArray registerBitArray

// ValidateRegsiters validate exposed columns do match filter names
func validateRegisters() error {
	var i = ItemOut{}
	var filters = []string{"match", "contains", "startswith"}
	for _, c := range i.Columns() {
		for _, f := range filters {
			filterKey := f + "-" + c
			if _, ok := RegisterFuncMap[filterKey]; !ok {
				fmt.Println("missing in RegisterMap: " + filterKey)
			}
		}
	}
	return nil
}

// startswith filter Postcode
func FilterGeneralStartsWith(i *Item, s string) bool {
	return FilterPostcodeStartsWith(i, s) ||
		FilterVidStartsWith(i, s) ||
		FilterPidStartsWith(i, s) ||
		FilterGemeentenaamStartsWith(i, s) ||
		FilterNumidStartsWith(i, s) ||
		FilterBuurtcodeStartsWith(i, s) ||
		FilterBuurtnaamStartsWith(i, s) ||
		FilterGemeentecodeStartsWith(i, s) ||
		FilterStraatStartsWith(i, s) ||
		FilterWijkcodeStartsWith(i, s) ||
		FilterWijknaamStartsWith(i, s) ||
		FilterGebruiksdoelenStartsWith(i, s)
}

func init() {

	RegisterFuncMap = make(registerFuncType)
	RegisterGroupBy = make(registerGroupByFunc)
	RegisterGetters = make(registerGettersMap)
	RegisterReduce = make(registerReduce)

	// register search filter.
	RegisterFuncMap["search"] = FilterGeneralStartsWith

	//RegisterFuncMap["value"] = 'EDITYOURSELF'
	// example RegisterGetters["value"] = GettersEkey

	// register filters

	//register filters for Numid
	RegisterFuncMap["match-numid"] = FilterNumidMatch
	RegisterFuncMap["contains-numid"] = FilterNumidContains
	RegisterFuncMap["startswith-numid"] = FilterNumidStartsWith
	RegisterGetters["numid"] = GettersNumid
	RegisterGroupBy["numid"] = GettersNumid

	//register filters for Pid
	RegisterFuncMap["match-pid"] = FilterPidMatch
	RegisterFuncMap["contains-pid"] = FilterPidContains
	RegisterFuncMap["startswith-pid"] = FilterPidStartsWith
	RegisterGetters["pid"] = GettersPid
	RegisterGroupBy["pid"] = GettersPid

	//register filters for Vid
	RegisterFuncMap["match-vid"] = FilterVidMatch
	//RegisterFuncMap["contains-vid"] = FilterVidContains
	//RegisterFuncMap["startswith-vid"] = FilterVidStartsWith
	RegisterGetters["vid"] = GettersVid
	RegisterGroupBy["vid"] = GettersVid

	//register filters for Lid
	RegisterFuncMap["match-lid"] = FilterLidMatch
	//RegisterFuncMap["contains-lid"] = FilterLidContains
	//RegisterFuncMap["startswith-lid"] = FilterLidStartsWith
	RegisterGetters["lid"] = GettersLid
	RegisterGroupBy["lid"] = GettersLid

	//register filters for Sid
	RegisterFuncMap["match-sid"] = FilterSidMatch
	//RegisterFuncMap["contains-sid"] = FilterSidContains
	//RegisterFuncMap["startswith-sid"] = FilterSidStartsWith
	RegisterGetters["sid"] = GettersSid
	RegisterGroupBy["sid"] = GettersSid

	//register filters for Postcode
	RegisterFuncMap["match-postcode"] = FilterPostcodeMatch
	RegisterFuncMap["contains-postcode"] = FilterPostcodeContains
	RegisterFuncMap["startswith-postcode"] = FilterPostcodeStartsWith
	RegisterGetters["postcode"] = GettersPostcode
	RegisterGroupBy["postcode"] = GettersPostcode

	//register filters for Straat
	RegisterFuncMap["match-straat"] = FilterStraatMatch
	RegisterFuncMap["contains-straat"] = FilterStraatContains
	RegisterFuncMap["startswith-straat"] = FilterStraatStartsWith
	RegisterGetters["straat"] = GettersStraat
	RegisterGroupBy["straat"] = GettersStraat

	//register filters for Huisnummer
	RegisterFuncMap["match-huisnummer"] = FilterHuisnummerMatch
	RegisterFuncMap["contains-huisnummer"] = FilterHuisnummerContains
	RegisterFuncMap["startswith-huisnummer"] = FilterHuisnummerStartsWith
	RegisterGetters["huisnummer"] = GettersHuisnummer
	RegisterGroupBy["huisnummer"] = GettersHuisnummer

	//register filters for Huisletter
	RegisterFuncMap["match-huisletter"] = FilterHuisletterMatch
	RegisterFuncMap["contains-huisletter"] = FilterHuisletterContains
	RegisterFuncMap["startswith-huisletter"] = FilterHuisletterStartsWith
	RegisterGetters["huisletter"] = GettersHuisletter
	RegisterGroupBy["huisletter"] = GettersHuisletter

	//register filters for Huisnummertoevoeging
	RegisterFuncMap["match-huisnummertoevoeging"] = FilterHuisnummertoevoegingMatch
	RegisterFuncMap["contains-huisnummertoevoeging"] = FilterHuisnummertoevoegingContains
	RegisterFuncMap["startswith-huisnummertoevoeging"] = FilterHuisnummertoevoegingStartsWith
	RegisterGetters["huisnummertoevoeging"] = GettersHuisnummertoevoeging
	RegisterGroupBy["huisnummertoevoeging"] = GettersHuisnummertoevoeging

	//register filters for Oppervlakte
	// RegisterFuncMap["match-oppervlakte"] = FilterOppervlakteMatch
	// RegisterFuncMap["contains-oppervlakte"] = FilterOppervlakteContains
	// RegisterFuncMap["startswith-oppervlakte"] = FilterOppervlakteStartsWith
	RegisterGetters["oppervlakte"] = GettersOppervlakte
	RegisterGroupBy["oppervlakte"] = GettersOppervlakte

	//register filters for Woningequivalent
	// RegisterFuncMap["match-woningequivalent"] = FilterWoningequivalentMatch
	// RegisterFuncMap["contains-woningequivalent"] = FilterWoningequivalentContains
	// RegisterFuncMap["startswith-woningequivalent"] = FilterWoningequivalentStartsWith
	RegisterGetters["woningequivalent"] = GettersWoningequivalent
	RegisterGroupBy["woningequivalent"] = GettersWoningequivalent

	//register filters for Gebruiksdoelen
	RegisterFuncMap["match-gebruiksdoelen"] = FilterGebruiksdoelenMatch
	RegisterFuncMap["contains-gebruiksdoelen"] = FilterGebruiksdoelenContains
	RegisterFuncMap["startswith-gebruiksdoelen"] = FilterGebruiksdoelenStartsWith
	RegisterGetters["gebruiksdoelen"] = GettersGebruiksdoelen
	RegisterGroupBy["gebruiksdoelen"] = GettersGebruiksdoelen

	//register filters for PandBouwjaar
	RegisterFuncMap["match-pand_bouwjaar"] = FilterPandBouwjaarMatch
	//RegisterFuncMap["contains-pand_bouwjaar"] = FilterPandBouwjaarContains
	//RegisterFuncMap["startswith-pand_bouwjaar"] = FilterPandBouwjaarStartsWith
	RegisterGetters["pand_bouwjaar"] = GettersPandBouwjaar
	RegisterGroupBy["pand_bouwjaar"] = GettersPandBouwjaar

	//register filters for GemiddeldeWozWaardeWoning
	// RegisterFuncMap["match-pc6_gemiddelde_woz_waarde_woning"] = FilterGemiddeldeWozWaardeWoningMatch
	// RegisterFuncMap["contains-pc6_gemiddelde_woz_waarde_woning"] = FilterGemiddeldeWozWaardeWoningContains
	// RegisterFuncMap["startswith-pc6_gemiddelde_woz_waarde_woning"] = FilterGemiddeldeWozWaardeWoningStartsWith
	RegisterGetters["pc6_gemiddelde_woz_waarde_woning"] = GettersGemiddeldeWozWaardeWoning
	RegisterGroupBy["pc6_gemiddelde_woz_waarde_woning"] = GettersGemiddeldeWozWaardeWoning

	//register filters for GemiddeldeGemeenteWoz
	// RegisterFuncMap["match-gemiddelde_gemeente_woz"] = FilterGemiddeldeGemeenteWozMatch
	// RegisterFuncMap["contains-gemiddelde_gemeente_woz"] = FilterGemiddeldeGemeenteWozContains
	// RegisterFuncMap["startswith-gemiddelde_gemeente_woz"] = FilterGemiddeldeGemeenteWozStartsWith
	RegisterGetters["gemiddelde_gemeente_woz"] = GettersGemiddeldeGemeenteWoz
	RegisterGroupBy["gemiddelde_gemeente_woz"] = GettersGemiddeldeGemeenteWoz

	//register filters for Energieklasse
	RegisterFuncMap["match-energieklasse"] = FilterEnergieklasseMatch
	RegisterFuncMap["contains-energieklasse"] = FilterEnergieklasseContains
	RegisterFuncMap["startswith-energieklasse"] = FilterEnergieklasseStartsWith
	RegisterGetters["energieklasse"] = GettersEnergieklasse
	RegisterGroupBy["energieklasse"] = GettersEnergieklasse

	//register filters for WoningType
	RegisterFuncMap["match-woning_type"] = FilterWoningTypeMatch
	RegisterFuncMap["contains-woning_type"] = FilterWoningTypeContains
	RegisterFuncMap["startswith-woning_type"] = FilterWoningTypeStartsWith
	RegisterGetters["woning_type"] = GettersWoningType
	RegisterGroupBy["woning_type"] = GettersWoningType

	//register filters for PandGebruiksoppervlakte
	// RegisterFuncMap["match-pand_gebruiksoppervlakte"] = FilterPandGebruiksoppervlakteMatch
	// RegisterFuncMap["contains-pand_gebruiksoppervlakte"] = FilterPandGebruiksoppervlakteContains
	// RegisterFuncMap["startswith-pand_gebruiksoppervlakte"] = FilterPandGebruiksoppervlakteStartsWith
	RegisterGetters["pand_gebruiksoppervlakte"] = GettersPandGebruiksoppervlakte
	RegisterGroupBy["pand_gebruiksoppervlakte"] = GettersPandGebruiksoppervlakte

	//register filters for Sbicode
	RegisterFuncMap["match-sbicode"] = FilterSbicodeMatch
	RegisterFuncMap["contains-sbicode"] = FilterSbicodeContains
	RegisterFuncMap["startswith-sbicode"] = FilterSbicodeStartsWith
	RegisterGetters["sbicode"] = GettersSbicode
	RegisterGroupBy["sbicode"] = GettersSbicode

	//register filters for GasEanCount
	RegisterFuncMap["match-gas_eancount"] = FilterEancountMatch
	RegisterFuncMap["gte-gas_eancount"] = FilterEancountgte
	//RegisterFuncMap["contains-eancount"] = FilterEancountContains
	//RegisterFuncMap["startswith-eancount"] = FilterEancountStartsWith
	RegisterGetters["gas_ean_count"] = GettersGasEanCount
	RegisterGroupBy["gas_ean_count"] = GettersGasEanCount

	//register filters for Pc6GroupId2022
	RegisterFuncMap["match-pc6_group_id_2022"] = FilterPc6GroupId2022Match
	RegisterFuncMap["contains-pc6_group_id_2022"] = FilterPc6GroupId2022Contains
	RegisterFuncMap["startswith-pc6_group_id_2022"] = FilterPc6GroupId2022StartsWith
	RegisterGetters["pc6_group_id_2022"] = GettersPc6GroupId2022
	RegisterGroupBy["pc6_group_id_2022"] = GettersPc6GroupId2022

	//register filters for P6Gasm32022
	// RegisterFuncMap["match-p6_gasm3_2022"] = FilterP6Gasm32022Match
	// RegisterFuncMap["contains-p6_gasm3_2022"] = FilterP6Gasm32022Contains
	RegisterFuncMap["gte-p6_gasm3_2022"] = FilterP6Gasm32022gte
	RegisterFuncMap["lte-p6_gasm3_2022"] = FilterP6Gasm32022lte
	RegisterGetters["p6_gasm3_2022"] = GettersP6Gasm32022
	RegisterGroupBy["p6_gasm3_2022"] = GettersP6Gasm32022

	//register filters for P6Kwh2022
	// RegisterFuncMap["match-p6_kwh_2022"] = FilterP6Kwh2022Match
	// RegisterFuncMap["contains-p6_kwh_2022"] = FilterP6Kwh2022Contains
	// RegisterFuncMap["startswith-p6_kwh_2022"] = FilterP6Kwh2022StartsWith
	RegisterGetters["p6_kwh_2022"] = GettersP6Kwh2022
	RegisterGroupBy["p6_kwh_2022"] = GettersP6Kwh2022

	//register filters for KwhLeveringsrichting2022
	// RegisterFuncMap["match-kwh_leveringsrichting_2022"] = FilterKwhLeveringsrichting2022Match
	// RegisterFuncMap["contains-kwh_leveringsrichting_2022"] = FilterKwhLeveringsrichting2022Contains
	// RegisterFuncMap["startswith-kwh_leveringsrichting_2022"] = FilterKwhLeveringsrichting2022StartsWith
	RegisterGetters["kwh_leveringsrichting_2022"] = GettersKwhLeveringsrichting2022
	RegisterGroupBy["kwh_leveringsrichting_2022"] = GettersKwhLeveringsrichting2022

	//register filters for P6GrondbeslagM2
	//RegisterFuncMap["match-p6_grondbeslag_m2"] = FilterP6GrondbeslagM2Match
	//RegisterFuncMap["contains-p6_grondbeslag_m2"] = FilterP6GrondbeslagM2Contains
	//RegisterFuncMap["startswith-p6_grondbeslag_m2"] = FilterP6GrondbeslagM2StartsWith
	RegisterGetters["p6_grondbeslag_m2"] = GettersP6GrondbeslagM2
	RegisterGroupBy["p6_grondbeslag_m2"] = GettersP6GrondbeslagM2

	//register filters for P6GasAansluitingen2022
	// RegisterFuncMap["match-p6_gas_aansluitingen_2022"] = FilterP6GasAansluitingen2022Match
	// RegisterFuncMap["contains-p6_gas_aansluitingen_2022"] = FilterP6GasAansluitingen2022Contains
	// RegisterFuncMap["startswith-p6_gas_aansluitingen_2022"] = FilterP6GasAansluitingen2022StartsWith
	RegisterGetters["p6_gas_aansluitingen_2022"] = GettersP6GasAansluitingen2022
	RegisterGroupBy["p6_gas_aansluitingen_2022"] = GettersP6GasAansluitingen2022

	//register filters for Point
	//RegisterFuncMap["match-point"] = FilterPointMatch
	//RegisterFuncMap["contains-point"] = FilterPointContains
	//RegisterFuncMap["startswith-point"] = FilterPointStartsWith
	RegisterGetters["point"] = GettersPoint
	RegisterGroupBy["point"] = GettersPoint

	//register filters for Buurtcode
	RegisterFuncMap["match-buurtcode"] = FilterBuurtcodeMatch
	RegisterFuncMap["contains-buurtcode"] = FilterBuurtcodeContains
	RegisterFuncMap["startswith-buurtcode"] = FilterBuurtcodeStartsWith
	RegisterGetters["buurtcode"] = GettersBuurtcode
	RegisterGroupBy["buurtcode"] = GettersBuurtcode

	//register filters for Buurtnaam
	RegisterFuncMap["match-buurtnaam"] = FilterBuurtnaamMatch
	RegisterFuncMap["contains-buurtnaam"] = FilterBuurtnaamContains
	RegisterFuncMap["startswith-buurtnaam"] = FilterBuurtnaamStartsWith
	RegisterGetters["buurtnaam"] = GettersBuurtnaam
	RegisterGroupBy["buurtnaam"] = GettersBuurtnaam

	//register filters for Wijkcode
	RegisterFuncMap["match-wijkcode"] = FilterWijkcodeMatch
	RegisterFuncMap["contains-wijkcode"] = FilterWijkcodeContains
	RegisterFuncMap["startswith-wijkcode"] = FilterWijkcodeStartsWith
	RegisterGetters["wijkcode"] = GettersWijkcode
	RegisterGroupBy["wijkcode"] = GettersWijkcode

	//register filters for Wijknaam
	RegisterFuncMap["match-wijknaam"] = FilterWijknaamMatch
	RegisterFuncMap["contains-wijknaam"] = FilterWijknaamContains
	RegisterFuncMap["startswith-wijknaam"] = FilterWijknaamStartsWith
	RegisterGetters["wijknaam"] = GettersWijknaam
	RegisterGroupBy["wijknaam"] = GettersWijknaam

	//register filters for Gemeentecode
	RegisterFuncMap["match-gemeentecode"] = FilterGemeentecodeMatch
	RegisterFuncMap["contains-gemeentecode"] = FilterGemeentecodeContains
	RegisterFuncMap["startswith-gemeentecode"] = FilterGemeentecodeStartsWith
	RegisterGetters["gemeentecode"] = GettersGemeentecode
	RegisterGroupBy["gemeentecode"] = GettersGemeentecode

	//register filters for Gemeentenaam
	RegisterFuncMap["match-gemeentenaam"] = FilterGemeentenaamMatch
	RegisterFuncMap["contains-gemeentenaam"] = FilterGemeentenaamContains
	RegisterFuncMap["startswith-gemeentenaam"] = FilterGemeentenaamStartsWith
	RegisterGetters["gemeentenaam"] = GettersGemeentenaam
	RegisterGroupBy["gemeentenaam"] = GettersGemeentenaam

	//register filters for Provincienaam
	RegisterFuncMap["match-provincienaam"] = FilterProvincienaamMatch
	RegisterFuncMap["contains-provincienaam"] = FilterProvincienaamContains
	RegisterFuncMap["startswith-provincienaam"] = FilterProvincienaamStartsWith
	RegisterGetters["provincienaam"] = GettersProvincienaam
	RegisterGroupBy["provincienaam"] = GettersProvincienaam

	//register filters for Provinciecode
	RegisterFuncMap["match-provinciecode"] = FilterProvinciecodeMatch
	RegisterFuncMap["contains-provinciecode"] = FilterProvinciecodeContains
	RegisterFuncMap["startswith-provinciecode"] = FilterProvinciecodeStartsWith
	RegisterGetters["provinciecode"] = GettersProvinciecode
	RegisterGroupBy["provinciecode"] = GettersProvinciecode

	validateRegisters()

	/*
		RegisterFuncMap["match-ekey"] = FilterEkeyMatch
		RegisterFuncMap["contains-ekey"] = FilterEkeyContains
		// register startswith filters
		RegisterFuncMap["startswith-ekey"] = FilterEkeyStartsWith
		// register getters
		RegisterGetters["ekey"] = GettersEkey
		// register groupby
		RegisterGroupBy["ekey"] = GettersEkey

	*/

	// register reduce functions
	RegisterReduce["count"] = reduceCount
	RegisterReduce["woningequivalent"] = reduceWEQ
	RegisterReduce["eancodes"] = reduceEAN
}

type sortLookup map[string]func(int, int) bool

func createSort(items Items) sortLookup {

	sortFuncs := sortLookup{

		"numid":  func(i, j int) bool { return items[i].Numid < items[j].Numid },
		"-numid": func(i, j int) bool { return items[i].Numid > items[j].Numid },

		"pid":  func(i, j int) bool { return Pid.GetValue(items[i].Pid) < Pid.GetValue(items[j].Pid) },
		"-pid": func(i, j int) bool { return Pid.GetValue(items[i].Pid) > Pid.GetValue(items[j].Pid) },

		"vid":  func(i, j int) bool { return items[i].Vid < items[j].Vid },
		"-vid": func(i, j int) bool { return items[i].Vid > items[j].Vid },

		"lid":  func(i, j int) bool { return items[i].Lid < items[j].Lid },
		"-lid": func(i, j int) bool { return items[i].Lid > items[j].Lid },

		"sid":  func(i, j int) bool { return items[i].Sid < items[j].Sid },
		"-sid": func(i, j int) bool { return items[i].Sid > items[j].Sid },

		"postcode":  func(i, j int) bool { return items[i].Postcode < items[j].Postcode },
		"-postcode": func(i, j int) bool { return items[i].Postcode > items[j].Postcode },

		"straat":  func(i, j int) bool { return items[i].Straat < items[j].Straat },
		"-straat": func(i, j int) bool { return items[i].Straat > items[j].Straat },

		"huisnummer": func(i, j int) bool {
			return Huisnummer.GetValue(items[i].Huisnummer) < Huisnummer.GetValue(items[j].Huisnummer)
		},
		"-huisnummer": func(i, j int) bool {
			return Huisnummer.GetValue(items[i].Huisnummer) > Huisnummer.GetValue(items[j].Huisnummer)
		},

		"huisletter": func(i, j int) bool {
			return Huisletter.GetValue(items[i].Huisletter) < Huisletter.GetValue(items[j].Huisletter)
		},
		"-huisletter": func(i, j int) bool {
			return Huisletter.GetValue(items[i].Huisletter) > Huisletter.GetValue(items[j].Huisletter)
		},

		"huisnummertoevoeging": func(i, j int) bool {
			return Huisnummertoevoeging.GetValue(items[i].Huisnummertoevoeging) < Huisnummertoevoeging.GetValue(items[j].Huisnummertoevoeging)
		},
		"-huisnummertoevoeging": func(i, j int) bool {
			return Huisnummertoevoeging.GetValue(items[i].Huisnummertoevoeging) > Huisnummertoevoeging.GetValue(items[j].Huisnummertoevoeging)
		},

		"oppervlakte":  func(i, j int) bool { return items[i].Oppervlakte < items[j].Oppervlakte },
		"-oppervlakte": func(i, j int) bool { return items[i].Oppervlakte > items[j].Oppervlakte },

		"woningequivalent":  func(i, j int) bool { return items[i].Woningequivalent < items[j].Woningequivalent },
		"-woningequivalent": func(i, j int) bool { return items[i].Woningequivalent > items[j].Woningequivalent },

		"gebruiksdoelen": func(i, j int) bool {
			return Gebruiksdoelen.GetArrayValue(items[i].Gebruiksdoelen) < Gebruiksdoelen.GetArrayValue(items[j].Gebruiksdoelen)
		},
		"-gebruiksdoelen": func(i, j int) bool {
			return Gebruiksdoelen.GetArrayValue(items[i].Gebruiksdoelen) > Gebruiksdoelen.GetArrayValue(items[j].Gebruiksdoelen)
		},

		"pand_bouwjaar":  func(i, j int) bool { return items[i].PandBouwjaar < items[j].PandBouwjaar },
		"-pand_bouwjaar": func(i, j int) bool { return items[i].PandBouwjaar > items[j].PandBouwjaar },

		"pc6_gemiddelde_woz_waarde_woning": func(i, j int) bool {
			return items[i].Pc6GemiddeldeWozWaardeWoning < items[j].Pc6GemiddeldeWozWaardeWoning
		},
		"-pc6_gemiddelde_woz_waarde_woning": func(i, j int) bool {
			return items[i].Pc6GemiddeldeWozWaardeWoning > items[j].Pc6GemiddeldeWozWaardeWoning
		},

		"gemiddelde_gemeente_woz": func(i, j int) bool {
			return GemiddeldeGemeenteWoz.GetValue(items[i].GemiddeldeGemeenteWoz) < GemiddeldeGemeenteWoz.GetValue(items[j].GemiddeldeGemeenteWoz)
		},
		"-gemiddelde_gemeente_woz": func(i, j int) bool {
			return GemiddeldeGemeenteWoz.GetValue(items[i].GemiddeldeGemeenteWoz) > GemiddeldeGemeenteWoz.GetValue(items[j].GemiddeldeGemeenteWoz)
		},

		"pc6_eigendomssituatie_perc_koop": func(i, j int) bool {
			return items[i].Pc6EigendomssituatiePercKoop < items[j].Pc6EigendomssituatiePercKoop
		},
		"-pc6_eigendomssituatie_perc_koop": func(i, j int) bool {
			return items[i].Pc6EigendomssituatiePercKoop > items[j].Pc6EigendomssituatiePercKoop
		},

		"pc6_eigendomssituatie_perc_huur": func(i, j int) bool {
			return items[i].Pc6EigendomssituatiePercHuur < items[j].Pc6EigendomssituatiePercHuur
		},
		"-pc6_eigendomssituatie_perc_huur": func(i, j int) bool {
			return items[i].Pc6EigendomssituatiePercHuur > items[j].Pc6EigendomssituatiePercHuur
		},

		"pc6_eigendomssituatie_aantal_woningen_corporaties": func(i, j int) bool {
			return items[i].Pc6EigendomssituatieAantalWoningenCorporaties < items[j].Pc6EigendomssituatieAantalWoningenCorporaties
		},
		"-pc6_eigendomssituatie_aantal_woningen_corporaties": func(i, j int) bool {
			return items[i].Pc6EigendomssituatieAantalWoningenCorporaties > items[j].Pc6EigendomssituatieAantalWoningenCorporaties
		},

		"energieklasse": func(i, j int) bool {
			return Energieklasse.GetValue(items[i].Energieklasse) < Energieklasse.GetValue(items[j].Energieklasse)
		},
		"-energieklasse": func(i, j int) bool {
			return Energieklasse.GetValue(items[i].Energieklasse) > Energieklasse.GetValue(items[j].Energieklasse)
		},

		"woning_type": func(i, j int) bool {
			return WoningType.GetValue(items[i].WoningType) < WoningType.GetValue(items[j].WoningType)
		},
		"-woning_type": func(i, j int) bool {
			return WoningType.GetValue(items[i].WoningType) > WoningType.GetValue(items[j].WoningType)
		},

		"pand_gebruiksoppervlakte":  func(i, j int) bool { return items[i].PandGebruiksoppervlakte < items[j].PandGebruiksoppervlakte },
		"-pand_gebruiksoppervlakte": func(i, j int) bool { return items[i].PandGebruiksoppervlakte > items[j].PandGebruiksoppervlakte },

		"sbicode":  func(i, j int) bool { return Sbicode.GetValue(items[i].Sbicode) < Sbicode.GetValue(items[j].Sbicode) },
		"-sbicode": func(i, j int) bool { return Sbicode.GetValue(items[i].Sbicode) > Sbicode.GetValue(items[j].Sbicode) },

		"gas_ean_count":  func(i, j int) bool { return items[i].GasEanCount < items[j].GasEanCount },
		"-gas_ean_count": func(i, j int) bool { return items[i].GasEanCount > items[j].GasEanCount },

		"pc6_group_id_2022": func(i, j int) bool {
			return Pc6GroupId2022.GetValue(items[i].Pc6GroupId2022) < Pc6GroupId2022.GetValue(items[j].Pc6GroupId2022)
		},
		"-pc6_group_id_2022": func(i, j int) bool {
			return Pc6GroupId2022.GetValue(items[i].Pc6GroupId2022) > Pc6GroupId2022.GetValue(items[j].Pc6GroupId2022)
		},

		"p6_gasm3_2022":  func(i, j int) bool { return items[i].P6Gasm32022 < items[j].P6Gasm32022 },
		"-p6_gasm3_2022": func(i, j int) bool { return items[i].P6Gasm32022 > items[j].P6Gasm32022 },

		"p6_kwh_2022":  func(i, j int) bool { return items[i].P6Kwh2022 < items[j].P6Kwh2022 },
		"-p6_kwh_2022": func(i, j int) bool { return items[i].P6Kwh2022 > items[j].P6Kwh2022 },

		"kwh_leveringsrichting_2022":  func(i, j int) bool { return items[i].KwhLeveringsrichting2022 < items[j].KwhLeveringsrichting2022 },
		"-kwh_leveringsrichting_2022": func(i, j int) bool { return items[i].KwhLeveringsrichting2022 > items[j].KwhLeveringsrichting2022 },

		"p6_grondbeslag_m2":  func(i, j int) bool { return items[i].P6GrondbeslagM2 < items[j].P6GrondbeslagM2 },
		"-p6_grondbeslag_m2": func(i, j int) bool { return items[i].P6GrondbeslagM2 > items[j].P6GrondbeslagM2 },

		"p6_gas_aansluitingen_2022":  func(i, j int) bool { return items[i].P6GasAansluitingen2022 < items[j].P6GasAansluitingen2022 },
		"-p6_gas_aansluitingen_2022": func(i, j int) bool { return items[i].P6GasAansluitingen2022 > items[j].P6GasAansluitingen2022 },

		"point":  func(i, j int) bool { return items[i].Point < items[j].Point },
		"-point": func(i, j int) bool { return items[i].Point > items[j].Point },

		"buurtcode": func(i, j int) bool {
			return Buurtcode.GetValue(items[i].Buurtcode) < Buurtcode.GetValue(items[j].Buurtcode)
		},
		"-buurtcode": func(i, j int) bool {
			return Buurtcode.GetValue(items[i].Buurtcode) > Buurtcode.GetValue(items[j].Buurtcode)
		},

		"buurtnaam": func(i, j int) bool {
			return Buurtnaam.GetValue(items[i].Buurtnaam) < Buurtnaam.GetValue(items[j].Buurtnaam)
		},
		"-buurtnaam": func(i, j int) bool {
			return Buurtnaam.GetValue(items[i].Buurtnaam) > Buurtnaam.GetValue(items[j].Buurtnaam)
		},

		"wijkcode": func(i, j int) bool {
			return Wijkcode.GetValue(items[i].Wijkcode) < Wijkcode.GetValue(items[j].Wijkcode)
		},
		"-wijkcode": func(i, j int) bool {
			return Wijkcode.GetValue(items[i].Wijkcode) > Wijkcode.GetValue(items[j].Wijkcode)
		},

		"wijknaam": func(i, j int) bool {
			return Wijknaam.GetValue(items[i].Wijknaam) < Wijknaam.GetValue(items[j].Wijknaam)
		},
		"-wijknaam": func(i, j int) bool {
			return Wijknaam.GetValue(items[i].Wijknaam) > Wijknaam.GetValue(items[j].Wijknaam)
		},

		"gemeentecode": func(i, j int) bool {
			return Gemeentecode.GetValue(items[i].Gemeentecode) < Gemeentecode.GetValue(items[j].Gemeentecode)
		},
		"-gemeentecode": func(i, j int) bool {
			return Gemeentecode.GetValue(items[i].Gemeentecode) > Gemeentecode.GetValue(items[j].Gemeentecode)
		},

		"gemeentenaam": func(i, j int) bool {
			return Gemeentenaam.GetValue(items[i].Gemeentenaam) < Gemeentenaam.GetValue(items[j].Gemeentenaam)
		},
		"-gemeentenaam": func(i, j int) bool {
			return Gemeentenaam.GetValue(items[i].Gemeentenaam) > Gemeentenaam.GetValue(items[j].Gemeentenaam)
		},

		"provincienaam": func(i, j int) bool {
			return Provincienaam.GetValue(items[i].Provincienaam) < Provincienaam.GetValue(items[j].Provincienaam)
		},
		"-provincienaam": func(i, j int) bool {
			return Provincienaam.GetValue(items[i].Provincienaam) > Provincienaam.GetValue(items[j].Provincienaam)
		},

		"provinciecode": func(i, j int) bool {
			return Provinciecode.GetValue(items[i].Provinciecode) < Provinciecode.GetValue(items[j].Provinciecode)
		},
		"-provinciecode": func(i, j int) bool {
			return Provinciecode.GetValue(items[i].Provinciecode) > Provinciecode.GetValue(items[j].Provinciecode)
		},
	}
	return sortFuncs
}

func sortBy(items Items, sortingL []string) (Items, []string) {
	sortFuncs := createSort(items)

	for _, sortFuncName := range sortingL {
		sortFunc, ok := sortFuncs[sortFuncName]
		if ok {
			sort.Slice(items, sortFunc)
		}
	}

	// TODO must be nicer way
	keys := []string{}
	for key := range sortFuncs {
		keys = append(keys, key)
	}

	return items, keys
}
