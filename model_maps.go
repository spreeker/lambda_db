/*
	Transforming ItemsIn -> Items -> ItemsOut
	Where Items has column values as integers to save memmory
	maps are needed to restore integers back to the actual values.
	those are generated and stored here.
*/

package main

type ModelMaps struct {
	Pid                   MappedColumn
	Huisnummer            MappedColumn
	Huisletter            MappedColumn
	Huisnummertoevoeging  MappedColumn
	Gebruiksdoelen        MappedColumn
	GemiddeldeGemeenteWoz MappedColumn
	Energieklasse         MappedColumn
	WoningType            MappedColumn
	Sbicode               MappedColumn
	Pc6GroupId2022        MappedColumn
	Buurtcode             MappedColumn
	Buurtnaam             MappedColumn
	Wijkcode              MappedColumn
	Wijknaam              MappedColumn
	Gemeentecode          MappedColumn
	Gemeentenaam          MappedColumn
	Provincienaam         MappedColumn
	Provinciecode         MappedColumn
}

var BitArrays map[string]fieldBitarrayMap

var Pid MappedColumn
var Huisnummer MappedColumn
var Huisletter MappedColumn
var Huisnummertoevoeging MappedColumn
var Gebruiksdoelen MappedColumn
var GemiddeldeGemeenteWoz MappedColumn
var Energieklasse MappedColumn
var WoningType MappedColumn
var Sbicode MappedColumn
var Pc6GroupId2022 MappedColumn
var Buurtcode MappedColumn
var Buurtnaam MappedColumn
var Wijkcode MappedColumn
var Wijknaam MappedColumn
var Gemeentecode MappedColumn
var Gemeentenaam MappedColumn
var Provincienaam MappedColumn
var Provinciecode MappedColumn

func clearBitArrays() {
	BitArrays = make(map[string]fieldBitarrayMap)
}

func init() {
	clearBitArrays()
	setUpRepeatedColumns()
}

func setUpRepeatedColumns() {
	Pid = NewReapeatedColumn("pid")
	Huisnummer = NewReapeatedColumn("huisnummer")
	Huisletter = NewReapeatedColumn("huisletter")
	Huisnummertoevoeging = NewReapeatedColumn("huisnummertoevoeging")
	Gebruiksdoelen = NewReapeatedColumn("gebruiksdoelen")
	GemiddeldeGemeenteWoz = NewReapeatedColumn("gemiddelde_gemeente_woz")
	Energieklasse = NewReapeatedColumn("energieklasse")
	WoningType = NewReapeatedColumn("woning_type")
	Sbicode = NewReapeatedColumn("sbicode")
	Pc6GroupId2022 = NewReapeatedColumn("pc6_group_id_2022")
	Buurtcode = NewReapeatedColumn("buurtcode")
	Buurtnaam = NewReapeatedColumn("buurtnaam")
	Wijkcode = NewReapeatedColumn("wijkcode")
	Wijknaam = NewReapeatedColumn("wijknaam")
	Gemeentecode = NewReapeatedColumn("gemeentecode")
	Gemeentenaam = NewReapeatedColumn("gemeentenaam")
	Provincienaam = NewReapeatedColumn("provincienaam")
	Provinciecode = NewReapeatedColumn("provinciecode")

}

func CreateMapstore() ModelMaps {
	return ModelMaps{
		Pid,
		Huisnummer,
		Huisletter,
		Huisnummertoevoeging,
		Gebruiksdoelen,
		GemiddeldeGemeenteWoz,
		Energieklasse,
		WoningType,
		Sbicode,
		Pc6GroupId2022,
		Buurtcode,
		Buurtnaam,
		Wijkcode,
		Wijknaam,
		Gemeentecode,
		Gemeentenaam,
		Provincienaam,
		Provinciecode,
	}
}

func LoadMapstore(m ModelMaps) {

	Pid = m.Pid
	Huisnummer = m.Huisnummer
	Huisletter = m.Huisletter
	Huisnummertoevoeging = m.Huisnummertoevoeging
	Gebruiksdoelen = m.Gebruiksdoelen
	GemiddeldeGemeenteWoz = m.GemiddeldeGemeenteWoz
	Energieklasse = m.Energieklasse
	WoningType = m.WoningType
	Sbicode = m.Sbicode
	Pc6GroupId2022 = m.Pc6GroupId2022
	Buurtcode = m.Buurtcode
	Buurtnaam = m.Buurtnaam
	Wijkcode = m.Wijkcode
	Wijknaam = m.Wijknaam
	Gemeentecode = m.Gemeentecode
	Gemeentenaam = m.Gemeentenaam
	Provincienaam = m.Provincienaam
	Provinciecode = m.Provinciecode

	RegisteredColumns[Pid.Name] = Pid
	RegisteredColumns[Huisnummer.Name] = Huisnummer
	RegisteredColumns[Huisletter.Name] = Huisletter
	RegisteredColumns[Huisnummertoevoeging.Name] = Huisnummertoevoeging
	RegisteredColumns[Gebruiksdoelen.Name] = Gebruiksdoelen
	RegisteredColumns[GemiddeldeGemeenteWoz.Name] = GemiddeldeGemeenteWoz
	RegisteredColumns[Energieklasse.Name] = Energieklasse
	RegisteredColumns[WoningType.Name] = WoningType
	RegisteredColumns[Sbicode.Name] = Sbicode
	RegisteredColumns[Pc6GroupId2022.Name] = Pc6GroupId2022
	RegisteredColumns[Buurtcode.Name] = Buurtcode
	RegisteredColumns[Buurtnaam.Name] = Buurtnaam
	RegisteredColumns[Wijkcode.Name] = Wijkcode
	RegisteredColumns[Wijknaam.Name] = Wijknaam
	RegisteredColumns[Gemeentecode.Name] = Gemeentecode
	RegisteredColumns[Gemeentenaam.Name] = Gemeentenaam
	RegisteredColumns[Provincienaam.Name] = Provincienaam
	RegisteredColumns[Provinciecode.Name] = Provinciecode

}
