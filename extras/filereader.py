"""
Import csv configuration
"""

import csv


def supported_fileformats():
    return list(FILE_FORMATS.keys())


def create_reader(f, file_format):
    reader_data = FILE_FORMATS[file_format]
    print(reader_data.get("args", {}))
    return reader_data["func"](f, **reader_data.get("args", {}))


FILE_FORMATS = {
    "csv": {"func": csv.DictReader},
    "scsv": {"func": csv.DictReader, "args": {"delimiter": ";"}},
    "tsv": {"func": csv.DictReader, "args": {"delimiter": "\t", "quotechar": '"'}},
}
