# STEP 1 build binary
FROM golang:1.21-alpine3.18 AS builder

RUN apk update && apk add --no-cache git tzdata
RUN apk --no-cache add ca-certificates

# Create appuser
ENV USER=appuser
ENV UID=10001

# See https://stackoverflow.com/a/55757473/12429735RUN
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

WORKDIR /app
COPY *.go /app/
COPY go.* /app/

# Fetch dependencies.
RUN go get -d -v

# Build the binary.
RUN CGO_ENABLED=0 GOOS=linux go build -o main

# STEP 2 build a small image
FROM scratch

COPY /files/name.bytesz /app/files/name.bytesz

# Copy static executable and certificates
COPY --from=builder /app/main /app/main
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
ENV TZ=Europe/Amsterdam

COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

# Use an unprivileged user.
USER appuser:appuser

COPY /files/www /app/files/www

WORKDIR /app

# Run the binary.

ENV http_db_host "0.0.0.0:8000"
ENV mgmt "n"
ENV prometheus-monitoring "n"
ENV indexed "n"
ENV GOGC 65
ENV LOADATSTARTUP "y"
# GOMEMLIMIT set it on deployment.
ENTRYPOINT ["/app/main"]
