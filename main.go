package main

import (
	"fmt"
	"net"

	//"github.com/prometheus/client_golang/prometheus"
	//"github.com/prometheus/client_golang/prometheus/promauto"
	"log"
	"net/http" //	"runtime/debug" "github.com/pkg/profile")
	"time"

	"golang.org/x/net/netutil"

	. "github.com/Attumm/settingo/settingo"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// Colors are fun, and can be used to note that this is joyfull and fun project.
const (
	//InfoColor    = "\033[1;34m%s\033[0m"
	//NoticeColor  = "\033[1;36m%s\033[0m"
	WarningColor = "\033[1;33m%s\033[0m"
	//ErrorColor   = "\033[1;31m%s\033[0m"
	//DebugColor   = "\033[0;36m%s\033[0m"
	GreenColorN   = "\033[1;32m%s\033[0m\n"
	InfoColorN    = "\033[1;34m%s\033[0m\n"
	NoticeColorN  = "\033[1;36m%s\033[0m\n"
	WarningColorN = "\033[1;33m%s\033[0m\n"
	ErrorColorN   = "\033[1;31m%s\033[0m\n"
	WTFColorN     = "\033[0;93m%s\033[0m\n"
)

var APIVERSION string

func init() {
	itemChan = make(ItemsChannel, 1000)
	APIVERSION = "0.23.11"
}

func loadcsv(itemChan ItemsChannel) {
	log.Print("loading given csv")
	fmt.Println("using delimiter", SETTINGS.Get("delimiter"))

	err := importCSV(SETTINGS.Get("csv"), itemChan,
		false, true,
		SETTINGS.Get("delimiter"),
		SETTINGS.Get("null-delimiter"))

	if err != nil {
		log.Print(err)
	}

	// make sure channels are empty
	// add timeout there is no garantee ItemsChannel
	// is empty and you miss a few records
	timeout, _ := time.ParseDuration(SETTINGS.Get("channelwait"))
	time.Sleep(timeout)
	// S2CELLS.Sort()
	fmt.Println("csv imported")

	ITEMS.FillIndexes()

	// Empty cache. should be made more generic
	cacheLock.Lock()
	defer cacheLock.Unlock()
	GroupByBodyCache = make(map[string]GroupByResult)
	GroupByHeaderCache = make(map[string]HeaderData)
}

func defaultSettings() {
	SETTINGS.Set("http_db_host", "0.0.0.0:8000", "host with port")
	SETTINGS.Set("SHAREDSECRET", "", "jwt shared secret")
	SETTINGS.Set("JWTENABLED", "n", "JWT enabled")

	SETTINGS.SetBool("CORS", true, "CORS enabled")

	SETTINGS.Set("csv", "", "load a gzipped csv file on starup")
	SETTINGS.Set("null-delimiter", "\\N", "null delimiter")
	SETTINGS.Set("delimiter", ",", "delimiter")

	SETTINGS.SetBool("mgmt", true, "enable the management api's for lambdadb")
	SETTINGS.SetBool("debug", false, "Add memory debug information during run")

	SETTINGS.Set("indexed", "n", "is the data indexed, for more information read the documenation?")
	SETTINGS.Set("strict-mode", "y", "strict mode does not allow ingestion of invalid items and will reject the batch")

	SETTINGS.Set("prometheus-monitoring", "n", "add promethues monitoring endpoint")
	SETTINGS.Set("STORAGEMETHOD", "bytesz", "Storagemethod available options are json, jsonz, bytes, bytesz")
	SETTINGS.SetBool("LOADATSTARTUP", false, "Load data at startup. ('y', 'n')")

	SETTINGS.SetBool("readonly", true, "only allow read only funcions")

	SETTINGS.Set("groupbycache", "yes", "use in memory cache")

	SETTINGS.SetBool("frontend", true, "Use Example frontend. ('y', 'n')")

	SETTINGS.Set("channelwait", "5s", "timeout")

	SETTINGS.Set("GOAT_KEY", "", "goat api key")

	SETTINGS.Parse()
}

func main() {

	defaultSettings()

	go ItemChanWorker(itemChan)

	if SETTINGS.Get("csv") != "" {
		go loadcsv(itemChan)
	}

	if SETTINGS.GetBool("debug") {
		go runPrintMem()
	}

	if SETTINGS.GetBool("LOADATSTARTUP") {
		log.Println("start loading")
		go loadAtStart(SETTINGS.Get("STORAGEMETHOD"), FILENAME, SETTINGS.GetBool("indexed"))
	}

	ipPort := SETTINGS.Get("http_db_host")

	mux := setupHandler()

	msg := fmt.Sprint(
		"starting server\nhost: ",
		ipPort,
	)
	log.SetFlags(log.Lmicroseconds)
	log.Printf(InfoColorN, msg)

	limitedListener, err := net.Listen("tcp", ipPort)
	if err != nil {
		log.Fatalf("Error starting server: %v", err)
	}

	limitListener := netutil.LimitListener(limitedListener, 5)

	srv := &http.Server{
		//ReadTimeout:  50 * time.Second,
		//WriteTimeout: 100 * time.Second,
		Handler: mux,
	}

	err = srv.Serve(limitListener)

	if err != nil {
		log.Fatalf("Error starting server: %v", err)
	}
}

func setupHandler() http.Handler {

	Operations = GroupedOperations{
		Funcs:     RegisterFuncMap,
		GroupBy:   RegisterGroupBy,
		Getters:   RegisterGetters,
		Reduce:    RegisterReduce,
		BitArrays: RegisterBitArray,
	}

	searchRest := contextSearchRest(itemChan, Operations)
	typeAheadRest := contextTypeAheadRest(itemChan, Operations)
	listRest := contextListRest(itemChan, Operations)
	addRest := contextAddRest(itemChan, Operations)

	mux := http.NewServeMux()

	mux.HandleFunc("/search/", searchRest)
	mux.HandleFunc("/typeahead/", typeAheadRest)
	mux.HandleFunc("/list/", listRest)
	mux.HandleFunc("/help/", helpRest)

	fs := http.FileServer(http.Dir("./files/www"))
	if SETTINGS.GetBool("frontend") {
		mux.Handle("/", fs)
	}

	if SETTINGS.GetBool("mgmt") {
		mux.HandleFunc("/mgmt/add/", addRest)
		mux.HandleFunc("/mgmt/rm/", rmRest)
		mux.HandleFunc("/mgmt/save/", saveRest)
		mux.HandleFunc("/mgmt/load/", loadRest)
	}

	if SETTINGS.GetBool("prometheus-monitoring") {
		mux.Handle("/metrics", promhttp.Handler())
	}

	fmt.Println("indexed: ", SETTINGS.Get("indexed"))

	cors := SETTINGS.GetBool("CORS")

	middleware := MIDDLEWARE(cors)

	msg := fmt.Sprint(
		"setup http handler:",
		" with:", len(ITEMS), "items ",
		"management api's: ", SETTINGS.GetBool("mgmt"),
		" monitoring: ", SETTINGS.Get("prometheus-monitoring") == "yes", " CORS: ", cors)

	fmt.Printf(InfoColorN, msg)

	return middleware(mux)
}
