/*

Validate polygon so no line intesects.

*/

package main

import (
	"fmt"

	"github.com/go-spatial/geom"
)

// Returns the square of the distance between two points.
func distSq(p, q geom.Point) float64 {
	dx := float64(p.X() - q.X())
	dy := float64(p.Y() - q.Y())
	return dx*dx + dy*dy
}

// Finds the orientation of three points.
// Returns 0 if they are collinear, 1 if they are clockwise, and 2 if they are counterclockwise.
func orientation(p, q, r geom.Point) int {
	val := (q.Y()-p.Y())*(r.X()-q.X()) - (q.X()-p.X())*(r.Y()-q.Y())

	if val == 0 {
		return 0
	} else if val > 0 {
		return 1
	} else {
		return 2
	}
}

// Comparator function to sort points based on their polar angle with the reference point.
func compare(pivot, a, b geom.Point) bool {
	o := orientation(pivot, a, b)
	if o == 0 {
		return distSq(pivot, b) >= distSq(pivot, a)
	}
	return o == 2
}

// Computes the convex hull of a given set of points.
func convexHull(points []geom.Point) []geom.Point {
	n := len(points)

	if n < 3 {
		fmt.Printf("not enough points %v", points)
		return []geom.Point{}
	}

	// Find the leftmost point
	leftmost := 0
	for i := 1; i < n; i++ {
		if points[i].X() < points[leftmost].X() {
			leftmost = i
		} else if points[i].X() == points[leftmost].X() && points[i].Y() < points[leftmost].Y() {
			leftmost = i
		}
	}

	hull := []geom.Point{}
	p := leftmost
	q := 0
	for {
		hull = append(hull, points[p])
		q = (p + 1) % n

		for i := 0; i < n; i++ {
			if orientation(points[p], points[i], points[q]) == 2 {
				q = i
			}
		}

		p = q

		if p == leftmost {
			break
		}
	}

	hull = append(hull, hull[0])
	return hull
}

func ValidatePolygon(points []geom.Point) []geom.Point {
	// log.Println("IN  ", points)
	// first and last should be equal.
	if points[0].X() == points[len(points)-1].X() {
		// remove last point since convexhull makes polygon complete again.
		points = points[1:]
	}
	points = convexHull(points)
	// log.Println("OUT ", points)
	return points
}
